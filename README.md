# Gitops Open

This repo allow to install some services for a self hosted cloud infrastructure based on kubernetes.

## Requirements

This components are requirement for your cloud installation:
- Rancher fleet
- Cert manager

## Installed components

- Nginx ingress controller
- Nextcloud
- Loki stack including this components:
  - Grafana
  - Prometheus
  - Promail